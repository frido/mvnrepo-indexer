package frido.mvnrepo.indexer.artifact;

public class XmlParseException extends Exception {
    private static final long serialVersionUID = 4981940164592492894L;

	public XmlParseException(Throwable t) {
        super(t);
    }
}